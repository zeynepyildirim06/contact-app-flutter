import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/add_contact_page.dart';
import 'package:flutterapp/contact_page.dart';

import 'Model/contact.dart';

void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
   return MaterialApp(
     home:ContactPage()
   );
  }

}
