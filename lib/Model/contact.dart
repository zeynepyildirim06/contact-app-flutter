class Contact{
  int id;
  String name;
  String phoneNumber;
  String avatar;

  static List<Contact> contacts=[
    Contact(name: "Zeynep",phoneNumber: "0555 555 55 55",avatar:""),
    Contact(name: "Beyza",phoneNumber: "0555 555 55 55",avatar:""),
    Contact(name: "Furkan",phoneNumber: "0555 555 55 55",avatar:""),
    Contact(name: "Bahadır",phoneNumber: "0555 555 55 55",avatar:""),
    Contact(name: "Kadir",phoneNumber: "0555 555 55 55",avatar:""),
    Contact(name: "Neslihan",phoneNumber: "0555 555 55 55",avatar:""),
    Contact(name: "Sena",phoneNumber: "0555 555 55 55",avatar:""),
    Contact(name: "Alirıza",phoneNumber: "0555 555 55 55",avatar:""),
    Contact(name: "Elif",phoneNumber: "0555 555 55 55",avatar:""),
    Contact(name: "Fatih",phoneNumber: "0555 555 55 55",avatar:""),
  ];
  Contact({this.name, this.phoneNumber,this.avatar});

  Map<String,dynamic> toMap(){
    var map=Map<String,dynamic>();
    map["name"]=name;
    map["phone_number"]=phoneNumber;
    map["avatar"] = avatar;
    return map;
  }

  Contact.fromMap(Map<String,dynamic> map){
    id=map["id"];
    name = map["name"];
    phoneNumber = map["phone_number"];
    avatar = map["avatar"];
  }
}